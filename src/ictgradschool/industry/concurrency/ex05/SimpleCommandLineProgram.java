package ictgradschool.industry.concurrency.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.concurrency.examples.example03.Consumer;
import jdk.nashorn.internal.ir.Block;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


import static com.sun.java.accessibility.util.AWTEventMonitor.addKeyListener;
import static java.lang.Thread.currentThread;

public class SimpleCommandLineProgram {


    public SimpleCommandLineProgram() {

    }

    private void start() {
        //Need to loop over inside here to keep program going
        System.out.println("Please enter a prime number:");
        String input = Keyboard.readInput();
        PrimeFactorsTask pft = new PrimeFactorsTask(Long.parseLong(input));
       PrimeFactorsTask.TaskState currentState;
        //New thread and runnable created
        Thread primeThread = new Thread(pft);
        //Thread 1 begins running

        Thread cancellerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    String c = Keyboard.readInput();
                    if (c.equalsIgnoreCase("c")) {
                        primeThread.interrupt();
                        return;
                    }
                }
            }
        });

        primeThread.start();
        cancellerThread.start();

        try {
            primeThread.join();
//            cancellerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        pft.getPrimeFactors();

        System.out.println("Prime factors are: " + pft.getPrimeFactors());

    }


    public static void main(String[] args) {
        new SimpleCommandLineProgram().start();
    }


}
