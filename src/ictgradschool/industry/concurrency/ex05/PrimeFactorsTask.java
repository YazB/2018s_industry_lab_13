package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

//Remember to make Runnable an interface!
public class PrimeFactorsTask implements Runnable {

    //
    public long N;
    private List <Long> primeFactors;
    private TaskState currentState;


    public PrimeFactorsTask(long n) {
        this.N = n;
        this.primeFactors = new ArrayList <>();
        this.currentState = TaskState.INITIALZED;
    }

    @Override
    public void run() {
        //For each potential factor
        for (long i = 2; i * i <= N; i++) {
            //if factor is a factor of n, repeatedly divide out
            while (N % i == 0) {


                primeFactors.add(i);
                N = N / i;
            }
            if (Thread.currentThread().isInterrupted()) {
                currentState = TaskState.ABORTED;
                break;
            }
        }

        //if biggest factor only occurs once, n>1
        if (N > 1) {
            primeFactors.add(N);
        }

        this.currentState = TaskState.COMPLETED;
    }

    public long n() {
        return N;
    }

    public List <Long> getPrimeFactors() throws IllegalStateException {
        if (currentState != TaskState.COMPLETED) {
            throw new IllegalStateException("Not complete");
        }

        return primeFactors;
    }

    public TaskState getState() {
        return currentState;
    }

    public enum TaskState {
        INITIALZED, COMPLETED, ABORTED;
    }
}
