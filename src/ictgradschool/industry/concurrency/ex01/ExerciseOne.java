package ictgradschool.industry.concurrency.ex01;

public class ExerciseOne {
  public void start() throws InterruptedException {
      System.out.println("Creating Runnable");

      Runnable myRunnable = new Runnable() {
          @Override
          public void run() {
              for (int i = 0; i <1_000_000; i++) {
                  System.out.println(i);
                  if (Thread.currentThread().isInterrupted()) {
                    Thread.currentThread().stop();
                    //check this line with Cameron!! Not sure if should be here - couldn't think of what else to do...
                  }
              }
          }
      };

      Thread myThread = new Thread(myRunnable);
      myThread.start();
      System.out.println("Starting Runnable");

      Thread.sleep(500);

      System.out.println("Ending");
      myThread.interrupt();

      myThread.join();
      System.out.println("Ending");
      //Also check with Cameron if meant to end in different places each time?
  }

    public static void main(String[] args) throws InterruptedException {
        (new ExerciseOne()).start();
    }
}
