package ictgradschool.industry.concurrency.ex03;

import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Math.sqrt;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {
        class ValueTracker {
            //Now that we are running multiple threads, good idea to keep track of them!
            public long getNumInCircle() {
                return numInCircle;
            }

            public synchronized void incNumInCircle(long numInCircle) {
                //As method is synchronized, ONLY ONE THREAD can call this method at a time!
                this.numInCircle += numInCircle;
                //Increment
            }
            long numInCircle = 0;
            //These two methods were originally pre-generated getters and setters
        }

        ValueTracker vals = new ValueTracker();
        //Shortening method to be called

        final int NUM_THREADS = 25;
        //Can change integer to test speed

        Runnable unitOfWork = new Runnable() {
            //Yay! Generated runnable!!
            @Override
            public void run() {
                // ThreadLocalRandom is a non-thread-safe version of Random that's designed to be used on a single thread.
                // Because it lacks thread-safety (ant its associated overheads), it is noticeably faster than Math.random(),
                // even when used on a single thread.
                ThreadLocalRandom tlr = ThreadLocalRandom.current();

                long numInsideCircle = 0;

                long count = numSamples / NUM_THREADS;
                //to streamline code when runs - changed from Single Threaded class

                for (long i = 0; i < count; i++) {
                    double x = tlr.nextDouble();
                    double y = tlr.nextDouble();

                    double magX = 0.5 - x;
                    double magY = 0.5 - y;

                    if (sqrt(magX * magX + magY * magY) <= 0.5) {
                        numInsideCircle++;
                    }
                }

                vals.incNumInCircle(numInsideCircle);
                //Increment return outside of loop - MUCH FASTER this way!!
            }
        };




        Thread[] allThreads = new Thread[NUM_THREADS];
        //Establishing array

        for (int i = 0; i < allThreads.length; i++) {
            allThreads[i] = new Thread(unitOfWork);
        } //At the moment, works for any number of threads defined as "Final int NUM-THREADS" (see above)

        for (Thread allThread : allThreads) {
            //Need to understand for each loops!!!
            allThread.start();
        }

        for (Thread allThread : allThreads) {
            try {
                allThread.join();
            } catch (InterruptedException e) {
            }
        }

        return 4.0 * (double) vals.getNumInCircle() / (double) (numSamples / NUM_THREADS * NUM_THREADS);
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
