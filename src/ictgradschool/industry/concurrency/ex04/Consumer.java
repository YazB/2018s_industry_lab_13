package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    private BlockingQueue <Transaction> queue;
    private BankAccount account;

    public Consumer(BlockingQueue <Transaction> queue, BankAccount account) {
        this.queue = queue;
        this.account = account;
    }

    @Override
    public void run() {
        boolean shouldRun = true;

        while (shouldRun || !queue.isEmpty()){
            try {
                Transaction transaction = queue.take();
                SerialBankingApp.doTransaction(transaction, account);
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " was interrupted and is quitting gracefully.");
                shouldRun = false;
            }
        }
    }
}