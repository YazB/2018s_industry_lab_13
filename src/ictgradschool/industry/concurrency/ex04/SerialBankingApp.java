package ictgradschool.industry.concurrency.ex04;

import java.util.List;

/**
 * Class to implement serial processing of Transactions on a single 
 * BankAccount object. 
 * 
 * This program simple acquires a List of Transaction objects from 
 * TransactionGenerator and applies each Transaction to the BankAccount object.
 * The balance of the BankAccount is initially zero. Hacing applied all
 * Transactions to the account, SerialBankingApp displays the final balance of
 * the account.
 *
 */
public class SerialBankingApp {
	static void doTransaction(Transaction t, BankAccount a) {
		switch (t._type) {
			case Deposit:
				a.deposit(t._amountInCents);
				break;
			case Withdraw:
				a.withdraw(t._amountInCents);
				break;
		}
	}

	public static void main(String[] args) {
		
		// Acquire Transactions to process.
		List<Transaction> transactions = TransactionGenerator.readDataFile();
		
		// Create BankAccount object to operate on.
		BankAccount account = new BankAccount();

		// For each Transaction, apply it to the BankAccount instance.
		for (Transaction transaction : transactions) {
			doTransaction(transaction, account);
		}

		// Print the final balance after applying all Transactions. 
		System.out.println("Final balance: " + account.getFormattedBalance());
	}
}