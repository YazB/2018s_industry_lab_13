package ictgradschool.industry.concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {
    public static void main(String[] args) throws InterruptedException {
        // Acquire Transactions to process.
        List <Transaction> transactions = TransactionGenerator.readDataFile();

        // Create BankAccount object to operate on.
        BankAccount account = new BankAccount();

        final BlockingQueue <Transaction> queue = new ArrayBlockingQueue <>(10);

        System.out.println("Creating consumers!");
        List <Thread> consumers = new ArrayList <>();
        for (int i = 0; i < 2; i++) {
            Consumer c = new Consumer(queue, account);
            Thread t = new Thread(c, "Consumer #" + i); // Threads can be assigned an arbitrary name so we can keep track of them.
            consumers.add(t);
        }
        System.out.println("2 consumers OK and ready to go.");
        System.out.println();

        Thread producer = new Thread(new Runnable() {
            //This is thread one: the producer
            @Override
            public void run() {

// For each Transaction, apply it to the BankAccount instance.
                for (Transaction transaction : transactions) {
                    try {
                        queue.put(transaction);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        /*
          Start the producer and the consumers, wait for
          the producer to finish before continuing
         */
        producer.start();
        for (Thread t : consumers) {
            t.start();
        }
        producer.join();

        /* Indicate to the consumers that the producer is finished */
        for (Thread t : consumers) {
            t.interrupt();
        }

        /* Await completion of the consumers */
        for (Thread t : consumers) {
            t.join();
            System.out.println(t.getName() + " finished successfully.");
        }

        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + account.getFormattedBalance());
    }
}
